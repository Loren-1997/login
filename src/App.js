import React, { Component, useContext } from 'react';
import {AuthContext, AuthProvider }from './AuthContext';
import Header from './header';
import Login from './login';


function App () {
  const authContext = useContext(AuthContext);
   return (
      <div className="container">
        <Header/>
         {authContext.auth.email ? 'WELCOME ' : <Login/>} 
      </div>
    );
  }

function AppWithStore(){
  return(
  <AuthProvider>
    <App/>
  </AuthProvider>);
}

export default AppWithStore;
