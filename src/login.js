import React, { Component  ,useState, useContext} from 'react';
import { Formik, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';
import {AuthContext} from "./AuthContext";


class Login extends Component {
    
    onSubmit = (values) => {
      console.log(values)
    }
    schema = () => {
      const schema = yup.object().shape({
  
        email: yup.string().required(),
        password: yup.string().required(),
  
      })
      return schema;
    }
     
  
    form = (props) => {
      return <form onSubmit={props.handleSubmit}>
        <h2>LOGIN</h2>
  
        <label>Email</label><br />
        <Field name='email' type='email' placeholder='Enter Your Email...' />
        <br />
        < ErrorMessage name='email' />
        <br />
        <label>password</label><br />
        <Field name='password' type='password' placeholder='Enter Your password...' />
        <br />
        < ErrorMessage name='password' />
        <br />
        <br />
        <button type='onSubmit' className="btn " >Login</button>
      </form>
    }
   
  
    render(){
      return (
        <div className="Login">
          
          <Formik
            initialValues={{
              email: "",
              password: "",
  
            }}
            onSubmit={this.onSubmit}
            render={this.form}
            validationSchema={this.schema()}
          />
          
        </div>
      );
    }}
  
  // Login.contextType=AuthContext;
  export default Login;